var URLOpener = function (url) {
  this.url = url;
};

URLOpener.prototype.windowSpecification = {
  location  : 'no',
  toolbar   : 'no',
  scrollbars: 'no',
  menubar   : 'no',
  titlebar  : 'no',
  status    : 'no',
  resizable : 'yes',
  width     : 400,
  height    : 450
};

URLOpener.prototype.getSpecObjectAsString = function (specObject) {
  var resultString = '';
  var key;
  for (key in specObject) {
    resultString += key + '=' + specObject[key] + ', ';
  }
  return resultString.replace(/\,\s+$/, '');
};

URLOpener.prototype.getWindowOpenerCallback = function (target) {
  var url = this.url;
  var windowSpecification = URLOpener.prototype.windowSpecification;
  var callback = function () {
    window.open(url, target, URLOpener.prototype.getSpecObjectAsString(windowSpecification));
  };
  return callback;
};

URLOpener.prototype.validateURL = function (text) {
  if ((text === null) || (text.length === 0)) {
    return false;
  }
  text = text.replace(/^\s+|\s+$/gm, "");
  var URL_REGEX = /(?:ht|f)tps?\:\/\/\S+$/im;
  return text.match(URL_REGEX);
};

var getURL = function () {
  var message = 'Please enter a valid URL';
  var url = prompt(message);
  if (!URLOpener.prototype.validateURL(url)) {
    url = getURL();
  }
  return url;
};

var url = getURL();

var openWindow = new URLOpener(url).getWindowOpenerCallback('_blank');
this.addEventListener('load', openWindow);
